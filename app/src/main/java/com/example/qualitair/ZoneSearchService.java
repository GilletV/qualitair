package com.example.qualitair;

import android.util.Log;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.query.Select;
import com.example.qualitair.event.EventBusManager;
import com.example.qualitair.event.EventBusManagerMeasure;
import com.example.qualitair.event.SearchLocationsResultEvent;
import com.example.qualitair.event.SearchMeasurementsResultEvent;
import com.example.qualitair.event.SearchResultEvent;
import com.example.qualitair.model.Location.Location;
import com.example.qualitair.model.Location.LocationSearchResult;
import com.example.qualitair.model.Measure.Measure;
import com.example.qualitair.model.Measure.MeasureSearchResult;
import com.example.qualitair.model.Measure.ResultMeasure;
import com.example.qualitair.model.Zone.Zone;
import com.example.qualitair.model.Zone.ZoneSearchResult;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Service de l'application : regroupe l'ensemble des requetes et des appels api de l'application
 *
 */
public class ZoneSearchService {

    private static final long REFRESH_DELAY = 650;
    public static ZoneSearchService INSTANCE = new ZoneSearchService();
    private final ZoneSearchRESTService mZoneSearchRESTService;
    private ScheduledExecutorService mScheduler = Executors.newScheduledThreadPool(1);
    private ScheduledFuture mLastScheduleTask;

    private ZoneSearchService()
    {
        Gson gsonConverter = new GsonBuilder()
                .excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC)
                .serializeNulls()
                .excludeFieldsWithoutExposeAnnotation().create();

        // Create Retrofit client
        Retrofit retrofit = new Retrofit.Builder()
                // Using OkHttp as HTTP Client
                .client(new OkHttpClient())
                // Having the following as server URL
                .baseUrl("https://api.openaq.org/v1/")
                // Using GSON to convert from Json to Java
                .addConverterFactory(GsonConverterFactory.create(gsonConverter))
                .build();

        // Use retrofit to generate our REST service code
        mZoneSearchRESTService = retrofit.create(ZoneSearchRESTService.class);
    }

    /**
     * recherche des zones
     */
    public void searchZone()
    {
        //Requete en base de données local
        searchZoneFromDB();

        //Requete de l'api
        mZoneSearchRESTService.searchForZones("FR",10000).enqueue(new Callback<ZoneSearchResult>()
        {

            @Override
            public void onResponse(Call<ZoneSearchResult> call, Response<ZoneSearchResult> response) {

                if (response.body() != null && response.body().results != null)
                {
                    ActiveAndroid.beginTransaction();
                    //Pour chaque zone recuperée de l'api, on l'enregistre en local
                    for(Zone zone : response.body().results)
                    {
                        zone.save();
                    }
                    ActiveAndroid.setTransactionSuccessful();
                    ActiveAndroid.endTransaction();

                    //Requete en base de données local avec les nouvelles données enregistrées
                    searchZoneFromDB();
                }
            }

            @Override
            public void onFailure(Call<ZoneSearchResult> call, Throwable t) {
                Log.e("NETWORK ERROR", t.getMessage());
            }
        });
    }

    /**
     * Recherche de location
     *
     * @param location
     */
    public void searchForLocations(String location)
    {
        //Requete en base de données local
        searchLocationFromDB(location);

        //Requete de l'api
        mZoneSearchRESTService.searchForLocations(location,"FR",10000).enqueue(new Callback<LocationSearchResult>()
        {
            @Override
            public void onResponse(Call<LocationSearchResult> call, Response<LocationSearchResult> response) {

                if (response.body() != null && response.body().results != null)
                {
                    ActiveAndroid.beginTransaction();
                    for(Location location : response.body().results)
                    {
                        //Pour chaque location récupérée depuis l'api on test si on l'a déjà en base
                        List<Location> matchingTheLocationFromDB = new Select().
                                from(Location.class)
                                .where("location LIKE \""+location.location+"\"")
                                .orderBy("location")
                                .execute();

                        // si oui et que son attribut favoris est a true, on met l'attribut favoris de la location a true car elle va venir écraser l'ancienne version stockée en base
                        if(matchingTheLocationFromDB != null && matchingTheLocationFromDB.size()>0 && matchingTheLocationFromDB.get(0) != null && matchingTheLocationFromDB.get(0).favori != null && matchingTheLocationFromDB.get(0).favori == true)
                        {
                            location.favori = true;
                        }
                        //Sinon on le mets a faux
                        else
                        {
                            location.favori = false;
                        }

                        location.save();
                        location.coordinates.identifiant = location.id;
                        location.coordinates.save();

                    }
                    ActiveAndroid.setTransactionSuccessful();
                    ActiveAndroid.endTransaction();

                    //Requete en base de données local avec les nouvelles données enregistrées
                    searchLocationFromDB(location);
                }


            }

            @Override
            public void onFailure(Call<LocationSearchResult> call, Throwable t) {
                Log.e("NETWORK ERROR", t.getMessage());
            }
        });
    }

    /**
     * Permet de récupérer les locations ajoutées en favoris par l'utilisateur
     */
    public void searchForLocationsFavoris()
    {
        //Requete en base de données local
        searchLocationFavorisFromDB();
    }

    /**
     * Perrmet de récupérer les mesures correspondant a la location, l'index pemet de retrouver a quel location appartient la mesure vu que les appels sont asynchrones
     *
     * @param location
     * @param locationName
     * @param index
     */
    public void searchForMeasurements(String location ,String locationName, int index)
    {
        //Requete en base de données local
        searchMeasureFromDB(locationName,index);

        //Requete de l'api
        mZoneSearchRESTService.searchForMeasurements(location).enqueue(new Callback<MeasureSearchResult>()
        {
            @Override
            public void onResponse(Call<MeasureSearchResult> call, Response<MeasureSearchResult> response) {
                if (response.body() != null && response.body().results != null )
                {
                    ActiveAndroid.beginTransaction();
                    for(Measure measure : response.body().results.get(0).measurements)
                    {
                        //il faut set la location a la main pour quelle puisse etrre enrergistrée en base
                        measure.location = response.body().results.get(0).location;
                        measure.save();
                    }

                    ActiveAndroid.setTransactionSuccessful();
                    ActiveAndroid.endTransaction();

                    //Requete en base de données local avec les nouvelles données enregistrées
                    searchMeasureFromDB(locationName,index);
                }
            }

            @Override
            public void onFailure(Call<MeasureSearchResult> call, Throwable t) {
                Log.e("NETWORK ERROR", t.getMessage());
            }
        });

    }

    /**
     * Permet de requeter en local pour récupérer toutes les zones
     */
    private void searchZoneFromDB() {
    // Get places matching the search from DB
        List<Zone> matchingZonesFromDB = new Select().
        from(Zone.class)
        .where("name LIKE '%'")
        .orderBy("name")
        .execute();
        // Post result as an event
        EventBusManager.BUS.post(new SearchResultEvent(matchingZonesFromDB));
    }

    /**
     * Permet de requeter en local pour récupérer les locations correspond a la zone passée en paramètre
     * @param location
     */
    private void searchLocationFromDB(String location) {
        // Get places matching the search from DB
        List<Location> matchingLocationsFromDB = new Select().
                from(Location.class)
                .where("city LIKE \""+location+"\"")
                .orderBy("city")
                .execute();
        // Post result as an event
        EventBusManager.BUS.post(new SearchLocationsResultEvent(matchingLocationsFromDB));
    }

    /**
     * Permet de requeter en local pour récupérer les locations mises en favoris par l'utilisateur
     */
    private void searchLocationFavorisFromDB() {
        // Get places matching the search from DB
        List<Location> matchingLocationFavorisFromDB = new Select().
                from(Location.class)
                .where("favori")
                .orderBy("location")
                .execute();
        // Post result as an event
        EventBusManager.BUS.post(new SearchLocationsResultEvent(matchingLocationFavorisFromDB));
    }

    /**
     * Permet de récupérer en local toute les mesures corrrepondant a la location passée en paramètre
     *
     * @param location
     * @param index
     */
    private void searchMeasureFromDB(String location,int index) {
        // Get places matching the search from DB
        List<Measure> matchingMeasuresFromDB = new Select().
                from(Measure.class)
                .where("location LIKE \""+location+"\"")
                .orderBy("location")
                .execute();
        // Post result as an event
        EventBusManager.BUS.post(new SearchMeasurementsResultEvent(matchingMeasuresFromDB,index));
    }


    /**
     * Service detaillant l'ensemble des appels API
     */
    public interface ZoneSearchRESTService {

        @GET("cities")
        Call<ZoneSearchResult> searchForZones(@Query("country") String search,@Query("limit") Number limite);

        @GET("locations")
        Call<LocationSearchResult> searchForLocations(@Query("city[]") String ville, @Query("country") String pays,@Query("limit") Number limite);

        @GET("latest")
        Call<MeasureSearchResult> searchForMeasurements(@Query("coordinates") String location);
    }
}
