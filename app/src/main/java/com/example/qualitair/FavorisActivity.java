package com.example.qualitair;

import android.os.Bundle;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.activeandroid.query.Select;
import com.example.qualitair.event.EventBusManager;
import com.example.qualitair.event.EventBusManagerMeasure;
import com.example.qualitair.event.SearchLocationsResultEvent;
import com.example.qualitair.event.SearchMeasurementsResultEvent;
import com.example.qualitair.model.Location.Location;
import com.example.qualitair.ui.LocationAdapter;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activité des locations favoris
 */
public class FavorisActivity extends AppCompatActivity {

    @BindView(R.id.activity_locations_recyclerView)
    RecyclerView mRecyclerView;

    private LocationAdapter mLocationAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favoris);

        ButterKnife.bind(this);

        mLocationAdapter = new LocationAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mLocationAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // Register to Event bus : now each time an event is posted, the activity will receive it if it is @Subscribed to this event
        EventBusManager.BUS.register(this);
        EventBusManagerMeasure.MeasureBUS.register(this);

        // lance une recherche en base local pou recuperer les locations favoris
        ZoneSearchService.INSTANCE.searchForLocationsFavoris();

    }

    @Subscribe
    public void searchResult(final SearchLocationsResultEvent event) {
        // Here someone has posted a SearchResultEvent
        runOnUiThread (() -> {
            // Step 1: Update adapter's model
            mLocationAdapter.setmLocations(event.getLocations());
            mLocationAdapter.notifyDataSetChanged();

            int i=0;
            String coordonnes;

            //Pour chaque location, on va récuperer les mesures correspondantes
            for(Location location : event.getLocations())
            {
                coordonnes = location.getCoordinates().latitude+","+location.getCoordinates().longitude;
                ZoneSearchService.INSTANCE.searchForMeasurements(coordonnes,location.location,i);
                i++;
            }
        });

    }

    @Subscribe
    public void searchResult(final SearchMeasurementsResultEvent event) {
        // Here someone has posted a SearchResultEvent
        runOnUiThread (() -> {
            // Step 1: Update adapter's model
            mLocationAdapter.getmLocations().get(event.getIndex()).measures = event.getMeasures();
            mLocationAdapter.notifyDataSetChanged();
        });

    }


}
