package com.example.qualitair;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.qualitair.event.EventBusManager;
import com.example.qualitair.event.EventBusManagerMeasure;
import com.example.qualitair.event.SearchLocationsResultEvent;
import com.example.qualitair.event.SearchMeasurementsResultEvent;
import com.example.qualitair.event.SearchResultEvent;
import com.example.qualitair.model.Location.Location;
import com.example.qualitair.ui.LocationAdapter;
import com.example.qualitair.ui.ZoneAdapter;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activité représentant la liste des locations
 */
public class LocationActivity extends AppCompatActivity {

    @BindView(R.id.activity_locations_recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.activity_locations_search_zone_edittext)
    EditText mSearchEditText;

    private LocationAdapter mLocationAdapter;

    private String cityname;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations);

        ButterKnife.bind(this);

        cityname = getIntent().getStringExtra("city");

        mLocationAdapter = new LocationAdapter(this, new ArrayList<>());
        mRecyclerView.setAdapter(mLocationAdapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mSearchEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                mLocationAdapter.getFilter().filter(editable.toString());
            }
        });

    }

    @Override
    protected void onResume()
    {
        super.onResume();

        // Register to Event bus : now each time an event is posted, the activity will receive it if it is @Subscribed to this event
        EventBusManager.BUS.register(this);
        EventBusManagerMeasure.MeasureBUS.register(this);

        // lance la recherche des locations dont la zone correspond au cityname passé en paramètre
        ZoneSearchService.INSTANCE.searchForLocations(cityname);
    }

    @Override
    protected void onPause() {
        // Unregister from Event bus : if event are posted now, the activity will not receive it
        EventBusManager.BUS.unregister(this);
        EventBusManagerMeasure.MeasureBUS.unregister(this);

        super.onPause();
    }

    @Subscribe
    public void searchResult(final SearchLocationsResultEvent event) {
        // Here someone has posted a SearchResultEvent
        runOnUiThread (() -> {
            // Step 1: Update adapter's model
            mLocationAdapter.setmLocations(event.getLocations());
            mLocationAdapter.notifyDataSetChanged();

            int i=0;
            String coordonnes;

            //Pour chaque location, on va récuperer les mesures correspondantes
            for(Location location : event.getLocations())
            {
                coordonnes = location.getCoordinates().latitude+","+location.getCoordinates().longitude;
                ZoneSearchService.INSTANCE.searchForMeasurements(coordonnes,location.location,i);
                i++;
            }
        });

    }

    @Subscribe
    public void searchResult(final SearchMeasurementsResultEvent event) {
        // Here someone has posted a SearchResultEvent
        runOnUiThread (() -> {
            // Step 1: Update adapter's model
            mLocationAdapter.getmLocations().get(event.getIndex()).measures = event.getMeasures();
            mLocationAdapter.notifyDataSetChanged();
        });

    }
}
