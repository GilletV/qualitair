package com.example.qualitair;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.activeandroid.ActiveAndroid;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Activité principal de l'application
 */
public class MainActivity extends AppCompatActivity {

    @BindView(R.id.Image_air)
    ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        ActiveAndroid.initialize(this);

        mImageView.setImageResource(R.drawable.air_icone_accueil);
    }

    //Click permettant d'accéder a l'activité des zones
    @OnClick(R.id.Button_zones)
    public void clickedOnZone()    {

        Intent seeZoneIntent = new Intent(this, ZoneActivity.class);
        startActivity(seeZoneIntent);
    }

    //Click permettant d'accéder a l'activité cartes
    @OnClick(R.id.button_carte)
    public void clickedOnCarte()
    {
        // Open place details activity
        Intent seeMapIntent = new Intent(this, MapActivity.class);
        startActivity(seeMapIntent);
    }

    //Click permettant d'accéder a l'activité des favoris
    @OnClick(R.id.Button_favoris)
    public void clickedOnFavoris()
    {
        // Open place details activity
        Intent seeFavorisIntent = new Intent(this, FavorisActivity.class);
        startActivity(seeFavorisIntent);
    }
}
