package com.example.qualitair.model.Measure;

import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

public class MeasureSearchResult implements Serializable {

    @Expose
    public List<ResultMeasure> results;
}
