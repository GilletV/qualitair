package com.example.qualitair.model.Location;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;
import com.example.qualitair.model.Measure.Measure;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

/**
 *
 * Modele d'une location
 *
 * id est l'identifiant de la location
 *
 * city correspond a la zone auquel la location appartient
 *
 * location est la clef en base et est le nom de location
 *
 * lastUpdated correspond à la date du dernier prélévement
 *
 * favoris correspond a la fonctionnalité permettant de mettre une lcoation en favori
 *
 * measures correspond a la liste des mesures sur la location
 *
 * coordinates correspond aux coordonnées de la location
 *
 */

@Table(name = "Location")
public class Location extends Model implements Serializable {

    @Expose
    @Column(name = "identifiant")
    public String id;

    @Column( name ="city")
    @Expose
    public String city;

    @Expose
    @Column(name = "location", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String location;

    @Column( name ="lastUpdated")
    @Expose
    public String lastUpdated;

    @Column( name ="favori")
    public Boolean favori;

    @Expose
    public List<Measure> measures;

    @Expose
    public Coordinates coordinates;

    public Coordinates getCoordinates()
    {
        if(coordinates == null)
        {
            coordinates =
                    new Select()
                    .from(Coordinates.class)
                    .where("identifiant LIKE \"" + id + "\"")
                    .executeSingle();
        }

        return coordinates;
    }

   public List<Measure> getMeasures()
    {
        if(measures == null)
        {
            measures =
                    new Select()
                            .from(Measure.class)
                            .where("location LIKE \"" + city + "\"")
                            .executeSingle();
        }

        return measures;
    }

}
