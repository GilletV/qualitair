package com.example.qualitair.model.Zone;

import com.example.qualitair.model.Zone.Zone;
import com.google.gson.annotations.Expose;

import java.util.List;

public class ZoneSearchResult {

    @Expose
    public List<Zone> results;
}
