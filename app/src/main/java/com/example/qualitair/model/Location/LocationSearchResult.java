package com.example.qualitair.model.Location;

import com.google.gson.annotations.Expose;

import java.util.List;

public class LocationSearchResult {

    @Expose
    public List<Location> results;
}
