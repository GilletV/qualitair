package com.example.qualitair.model.Measure;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
import java.util.List;

/**
 * Result measure est un model intermédiaire permettant de récuperer les valeurs des mesure depuis l'api Latest
 *
 * city represente la zone
 *
 * location represente la location
 *
 * measurement represente la liste des mesures
 *
 */
public class ResultMeasure implements Serializable {

    @Expose
    public String city;

    @Expose
    public String location;

    @Expose
    public List<Measure> measurements;
}
