package com.example.qualitair.model.Location;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.io.Serializable;
/**
 *
 * Modele des coordonnées d'une location
 *
 *  Identifiant est la clef en base
 *
 * Longitude represente la longitude
 *
 * latitude represente la latitude
 *
 */
@Table(name = "Coordinates")
public class Coordinates extends Model implements Serializable {

    @Column(name = "identifiant", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String identifiant;

    @Expose
    @Column( name ="longitude")
    public double longitude;

    @Expose
    @Column( name ="latitude")
    public double latitude;
}
