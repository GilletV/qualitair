package com.example.qualitair.model.Zone;


import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

/**
 * Zone est le model représentant le model des zones
 *
 * name est la clef en base et represente le nom de la zone
 *
 * city represente aussi le nom de la zone
 *
 * locations represente le nombre de location dans la zone
 *
 * count represente le nombre de mesure qui ont été prise sur la zone
 */

@Table(name = "Zone")
public class Zone extends Model {

    @Expose
    @Column(name = "name", index = true, unique = true, onUniqueConflict = Column.ConflictAction.REPLACE)
    public String name;

    @Expose
    @Column( name ="city")
    public String city;

    @Expose
    @Column( name ="locations")
    public Integer locations;

    @Expose
    @Column( name ="count")
    public Integer count;

    public Zone() {
        super();
    }

    @Override
    public String toString() {
        return "name : "+name+" city : "+city+" locations : "+locations+"count : "+count+" ";
    }
}
