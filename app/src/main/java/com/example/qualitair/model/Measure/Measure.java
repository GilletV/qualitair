package com.example.qualitair.model.Measure;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.google.gson.annotations.Expose;

import java.io.Serializable;

/**
 * Measure est le model est mesures prises sur les location
 *
 * Location est une des clefs en base, elle represente la location ou la mesure a été prise
 *
 * parameter est une des clefs en base, elle represente le parametre testé par la mesure
 *
 * value represente la valeur de la mesure
 *
 * unit represente l'unité de la valeur
 *
 */
public class Measure extends Model implements Serializable {

    @Column(name ="location", index = true, uniqueGroups = {"groupMeasure"}, onUniqueConflicts = {Column.ConflictAction.REPLACE})
    public String location;

    @Expose
    @Column(name ="parameter", index = true, uniqueGroups = {"groupMeasure"}, onUniqueConflicts = {Column.ConflictAction.REPLACE})
    public String parameter;

    @Expose
    @Column(name ="value")
    public Double value;

    @Expose
    @Column(name ="unit")
    public String unit;

    public Measure(){
        super();
    }

}
