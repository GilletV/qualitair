package com.example.qualitair.ui;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.qualitair.DetailLocationActivity;
import com.example.qualitair.LocationActivity;
import com.example.qualitair.R;
import com.example.qualitair.ZoneSearchService;
import com.example.qualitair.model.Location.Location;
import com.example.qualitair.model.Measure.Measure;
import com.example.qualitair.model.Zone.Zone;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;


/**
 * Adapater pour la recycler view des locations
 */
public class LocationAdapter extends RecyclerView.Adapter<LocationAdapter.LocationViewHolder> implements Serializable,Filterable {

    private LayoutInflater inflater;
    private Activity context;

    private List<Location> mLocations;

    //Cette variable sert de variable tampon qui va stocker tout les résultats avant le filtre.
    // Cela permet de remettre les résultats si l'on vide la zone de recherche
    private List<Location> mLocationsFull = new ArrayList<>();

    private List<Measure> mMesure;

    public LocationAdapter(Activity context, List<Location> Locations) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.mLocations = Locations;
    }

    @Override
    public LocationAdapter.LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.location_item, parent, false);
        LocationAdapter.LocationViewHolder holder = new LocationAdapter.LocationViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(LocationAdapter.LocationViewHolder holder, int position) {
        // Adapt the ViewHolder state to the new element
        final Location location = mLocations.get(position);
        holder.mLocationNameTextView.setText(location.location);
        holder.mLocationIcon.setImageResource(R.drawable.location_icon);

        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();
        try {
            date = inputFormat.parse(location.lastUpdated);
        }catch (ParseException p)
        {

        }
        String formattedDate = outputFormat.format(date);
        System.out.println(formattedDate);


        holder.mLocationDateTextView.setText("Dernier prélévement : " + formattedDate);

        holder.mLocationIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Open place details activity
                Intent seePlaceDetailIntent = new Intent(context, DetailLocationActivity.class);
                seePlaceDetailIntent.putExtra("location", location);
                context.startActivity(seePlaceDetailIntent);
            }
        });

        if(location.measures != null && !location.measures.isEmpty())
        {
            for(Measure measure : location.measures)
            {
                switch (measure.parameter) {
                    case "pm25":
                        holder.mLocationPM25Textview.setText(measure.parameter.toUpperCase() + " : " + measure.value + " " + measure.unit);
                        break;
                    case "pm10":
                        holder.mLocationPM10Textview.setText(measure.parameter.toUpperCase() + " : " + measure.value + " " + measure.unit);
                        break;
                    case "so2":
                        holder.mLocationSO2Textview.setText(measure.parameter.toUpperCase() + " : " + measure.value + " " + measure.unit);
                        break;
                    case "no2":
                        holder.mLocationNO2Textview.setText(measure.parameter.toUpperCase() + " : " + measure.value + " " + measure.unit);
                        break;
                    case "o3":
                        holder.mLocationO3Textview.setText(measure.parameter.toUpperCase() + " : " + measure.value + " " + measure.unit);
                        break;
                    case "co":
                        holder.mLocationCOTextview.setText(measure.parameter.toUpperCase() + " : " + measure.value + " " + measure.unit);
                        break;
                    case "bc":
                        holder.mLocationBCTextview.setText(measure.parameter.toUpperCase() + " :" + measure.value + " " + measure.value);
                        break;
                    default:

                }
            }
        }

  }

    @Override
    public int getItemCount() {
        return mLocations.size();
    }

    public void setmLocations(List<Location> locations) {
        this.mLocations = locations;
        this.mLocationsFull.clear();
        this.mLocationsFull.addAll(locations);
    }

    public List<Location> getmLocations() {
        return mLocations;
    }

    public void setmMesure(List<Measure> mesures)
    {
        this.mMesure = mesures;
    }


    // Pattern ViewHolder
    class LocationViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.location_adapter_name)
        TextView mLocationNameTextView;

        @BindView(R.id.location_adapter_date)
        TextView mLocationDateTextView;

        @BindView(R.id.location_adapter_pm25)
        TextView mLocationPM25Textview;

        @BindView(R.id.location_adapter_pm10)
        TextView mLocationPM10Textview;

        @BindView(R.id.location_adapter_so2)
        TextView mLocationSO2Textview;

        @BindView(R.id.location_adapter_no2)
        TextView mLocationNO2Textview;

        @BindView(R.id.location_adapter_o3)
        TextView mLocationO3Textview;

        @BindView(R.id.location_adapter_co)
        TextView mLocationCOTextview;

        @BindView(R.id.location_adapter_bc)
        TextView mLocationBCTextview;

        @BindView(R.id.location_adapter_icon)
        ImageView mLocationIcon;

        public LocationViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public Filter getFilter() {
        return locationFilter;
    }

    /**
     * Permet de filtrer les locations a partir de la zone de texte en haut de page
     */
    private Filter locationFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<Location> locationFiltre = new ArrayList<>();

            if(constraint == null || constraint.length() == 0)
            {
                locationFiltre.addAll(mLocationsFull);
            }
            else
            {
                String filtre = constraint.toString().toLowerCase().trim();

                for( Location location : mLocationsFull)
                {
                    if(location.location.toLowerCase().startsWith(filtre))
                    {
                        locationFiltre.add(location);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = locationFiltre;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mLocations.clear();
            mLocations.addAll((List)results.values);

            notifyDataSetChanged();
        }
    };

}
