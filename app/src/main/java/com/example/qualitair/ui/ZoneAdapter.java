package com.example.qualitair.ui;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.example.qualitair.LocationActivity;
import com.example.qualitair.R;
import com.example.qualitair.model.Zone.Zone;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapater pour la recycler view des zones
 */

public class ZoneAdapter extends RecyclerView.Adapter<ZoneAdapter.ZoneViewHolder> implements Filterable {

    private LayoutInflater inflater;
    private Activity context;

    private List<Zone> mZones;

    //Cette variable sert de variable tampon qui va stocker tout les résultats avant le filtre.
    //Cela permet de remettre les résultats si l'on vide la zone de recherche
    private List<Zone> mZonesFull = new ArrayList<>();

    public ZoneAdapter(Activity context, List<Zone> Zones) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.mZones = Zones;
    }

    @Override
    public ZoneAdapter.ZoneViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.zone_item, parent, false);
        ZoneAdapter.ZoneViewHolder holder = new ZoneAdapter.ZoneViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(ZoneAdapter.ZoneViewHolder holder, int position) {
        // Adapt the ViewHolder state to the new element
        final Zone zone = mZones.get(position);
        holder.mZoneNameTextView.setText(zone.name);
        holder.mZoneLocationsTextView.setText("Lieux : "+zone.locations);
        holder.mZoneCountTextView.setText("Nombre de mesure : "+zone.count);
        holder.mZoneIcon.setImageResource(R.drawable.zone_icon);



        holder.mZoneIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Open place details activity
                Intent seePlaceDetailIntent = new Intent(context, LocationActivity.class);
                seePlaceDetailIntent.putExtra("city", zone.name);
                context.startActivity(seePlaceDetailIntent);
            }
        });



    }

    @Override
    public int getItemCount() {
        return mZones.size();
    }

    public void setZones(List<Zone> zones) {
        this.mZones = zones;
        this.mZonesFull.clear();
        this.mZonesFull.addAll(zones);
    }

    // Pattern ViewHolder
    class ZoneViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.zone_adapter_name)
        TextView mZoneNameTextView;

        @BindView(R.id.zone_adapter_locations)
        TextView mZoneLocationsTextView;

        @BindView(R.id.zone_adapter_count)
        TextView mZoneCountTextView;

        @BindView(R.id.zone_adapter_icon)
        ImageView mZoneIcon;

        public ZoneViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public Filter getFilter() {
        return zoneFilter;
    }

    private Filter zoneFilter = new Filter() {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            List<Zone> zoneFiltre = new ArrayList<>();

            if(constraint == null || constraint.length() == 0)
            {
                zoneFiltre.addAll(mZonesFull);
            }
            else
            {
                String filtre = constraint.toString().toLowerCase().trim();

                for( Zone zone : mZonesFull)
                {
                    if(zone.name.toLowerCase().startsWith(filtre))
                    {
                        zoneFiltre.add(zone);
                    }
                }
            }

            FilterResults results = new FilterResults();
            results.values = zoneFiltre;

            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            mZones.clear();
            mZones.addAll((List)results.values);

            notifyDataSetChanged();
        }
    };
}