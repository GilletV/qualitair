package com.example.qualitair;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.example.qualitair.event.EventBusManager;
import com.example.qualitair.event.EventBusManagerMeasure;
import com.example.qualitair.event.SearchLocationsResultEvent;
import com.example.qualitair.event.SearchMeasurementsResultEvent;
import com.example.qualitair.event.SearchResultEvent;
import com.example.qualitair.model.Location.Location;
import com.example.qualitair.model.Measure.Measure;
import com.example.qualitair.model.Zone.Zone;
import com.example.qualitair.ui.LocationAdapter;
import com.example.qualitair.ui.ZoneAdapter;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import butterknife.ButterKnife;

/**
 * Activité de la carte permettant d'afficher les zones puis les locations correspondantes
 */

public class MapActivity extends AppCompatActivity implements OnMapReadyCallback {

    private GoogleMap mActiveGoogleMap;
    private Map<String,String> mMarkersToPlaces = new LinkedHashMap<>();
    private Map<String,Location> mMarkersLocationToPlaces = new LinkedHashMap<>();

    private List<Zone> zones = new ArrayList<Zone>();
    private List<Location> locations = new ArrayList<Location>();
    private Location location;

    /* Est passe par zone est une variable booleenne permettant
     * de savoir si on dans le cas ou l'on affiche les zones
     * ou que l'on a cliqué sur une zone et qu'on affiche
     * les zones correspondantes
     */
    private boolean estPasseParZone;

    private Activity context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map_location);

        ButterKnife.bind(this);

        context = this;

        // Get map fragment
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    protected void onResume() {
        // Do NOT forget to call super.onResume()
        super.onResume();

        estPasseParZone = false;

        EventBusManager.BUS.register(this);
        EventBusManagerMeasure.MeasureBUS.register(this);


   }

    @Override
    protected void onPause() {
        EventBusManager.BUS.unregister(this);
        EventBusManagerMeasure.MeasureBUS.unregister(this);

        super.onPause();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mActiveGoogleMap = googleMap;
        mActiveGoogleMap.getUiSettings().setZoomControlsEnabled(true);
        mActiveGoogleMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);

        ZoneSearchService.INSTANCE.searchZone();

        mActiveGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {

                //Est passé par zone pemet de savoir si le click a été fait sur une zone ou sur une location
                if(estPasseParZone == true) {

                    //le click a été fait sur une zone il faut donc récupérer tout les locations correspondantes

                    String associatedLocationCity = mMarkersToPlaces.get(marker.getId());
                    if (associatedLocationCity != null) {
                        mActiveGoogleMap.clear();
                        mMarkersToPlaces.clear();

                        estPasseParZone = false;

                        // lance une recherche pour obtenir la liste des locations
                        ZoneSearchService.INSTANCE.searchForLocations(associatedLocationCity);

                    }
                }
                else
                {
                    //Le click a été fait sur une location, il faut donc afficher son détail

                    Location locationIntent = mMarkersLocationToPlaces.get(marker.getId());
                    if (locationIntent != null) {
                        Intent seePlaceDetailIntent = new Intent(context , DetailLocationActivity.class);
                        seePlaceDetailIntent.putExtra("location", locationIntent);
                        startActivity(seePlaceDetailIntent);

                    }
                }
            }
        });
    }

    @Subscribe
    public void searchResult(final SearchResultEvent event) {
        // Here someone has posted a SearchResultEvent
        runOnUiThread (() -> {
            // Step 1: Update adapter's model
            zones.clear();
            zones.addAll(event.getZones());
            manipulationZones();
        });

    }

    @Subscribe
    public void searchLocationsResult(final SearchLocationsResultEvent event) {
        // Here someone has posted a SearchResultEvent
        runOnUiThread (() -> {
            // Step 1: Update adapter's model
            locations.clear();
            locations.addAll(event.getLocations());
            manipulationLocations();
        });

    }

    @Subscribe
    public void searchResult(final SearchMeasurementsResultEvent event) {
        // Here someone has posted a SearchResultEvent
        runOnUiThread (() -> {
            // Step 1: Update adapter's model
            List<Measure> measures = locations.get(event.getIndex()).getMeasures();

            locations.get(event.getIndex()).measures = event.getMeasures();
            Location associatedLocation = locations.get(event.getIndex());

            //String concaténant l'ensemble des mesures dans le but de l'afficher
            String lesMesures = new String();

            Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.location_icon);
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, 50, 50, false);

            for(Measure measure : associatedLocation.measures)
            {
                //On test si la mesure est déjà dans la variable pour éviter les doublons
                if(!lesMesures.contains(measure.parameter)) {
                    lesMesures = lesMesures + measure.parameter + " : " + measure.value + " " + measure.unit + " | ";
                }
            }

            //Création d'un marker d'une location
            MarkerOptions markerOptions = new MarkerOptions().position((new LatLng(associatedLocation.getCoordinates().latitude, associatedLocation.getCoordinates().longitude))).title(associatedLocation.location).snippet(lesMesures).icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap));

            Marker marker = mActiveGoogleMap.addMarker(markerOptions);
            mMarkersLocationToPlaces.put(marker.getId(),associatedLocation);
        });

    }

    /**
     * Methode appelée lorsque l'on récupère les zones de france
     *
     */
    public void manipulationZones()
    {
        estPasseParZone = true;
        if(!zones.isEmpty()) {
            for (Zone zone : zones) {

                /*lance une recherche pour obtenir la liste des locations,
                 * le but est de considerer les coordonnées de la première location
                 *  comme les coordonnées de la zone
                 */
                ZoneSearchService.INSTANCE.searchForLocations(zone.city);

            }
        }
    }


    /**
     * Methode appelée lors que l'on récupère les locations
     */
    public void manipulationLocations()
    {
        if (!locations.isEmpty()) {

            /*Dans le cas ou on est passé par zone
             * On veut juste récuperer les coordonnées de la première location de la zone pour les considérer
             * comme les coordnnées de la zone
             */
            if(estPasseParZone == true) {
                location = locations.get(0);

                Bitmap imageBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.zone_icon);
                Bitmap resizedBitmap = Bitmap.createScaledBitmap(imageBitmap, 50, 50, false);

                Double lat = location.getCoordinates().latitude;
                Double lon = location.getCoordinates().longitude;

                MarkerOptions markerOptions = new MarkerOptions().position((new LatLng(lat, lon))).title(location.city).icon(BitmapDescriptorFactory.fromBitmap(resizedBitmap));

                Marker marker = mActiveGoogleMap.addMarker(markerOptions);
                mMarkersToPlaces.put(marker.getId(),location.city);
            }
            /* Dans le cas ou l'on est pas passé par zone
             * cela signifie que l'on a cliqué sur une zone
             * et qu'on veut afficher les locations de la zone
             */
            else
            {
                int i=0;
                String coordonnes;
                for (Location location : locations) {

                    //On concatene les coordonnées pour les utiliser pour chercher les mesure dans l'api
                    coordonnes = location.getCoordinates().latitude+","+location.getCoordinates().longitude;

                    // lance une recherche pour obtenir la liste des measures correspondant a la location dont les coordonnées sont passées en paramètre
                    ZoneSearchService.INSTANCE.searchForMeasurements(coordonnes,location.location,i);
                    i++;
                }
            }

        }
    }

}
