package com.example.qualitair;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextClock;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.activeandroid.ActiveAndroid;
import com.example.qualitair.model.Location.Location;
import com.example.qualitair.model.Measure.Measure;
import com.example.qualitair.ui.LocationAdapter;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Activité de détail de la location
 */
public class DetailLocationActivity extends AppCompatActivity implements Serializable {

    @BindView(R.id.detail_location_image)
    ImageView detail_location_image;

    @BindView(R.id.detail_location_adapter_name)
    TextView detail_location_name;

    @BindView(R.id.detail_location_adapter_date)
    TextView detail_location_date;

    @BindView(R.id.detail_location_adapter_temperature)
    TextView detail_location_temperature;

    @BindView(R.id.detail_location_adapter_bc)
    TextView detail_location_bc;

    @BindView(R.id.detail_location_adapter_co)
    TextView detail_location_co;

    @BindView(R.id.detail_location_adapter_no2)
    TextView detail_location_no2;

    @BindView(R.id.detail_location_adapter_o3)
    TextView detail_location_o3;

    @BindView(R.id.detail_location_adapter_pm10)
    TextView detail_location_pm10;

    @BindView(R.id.detail_location_adapter_pm25)
    TextView detail_location_pm25;

    @BindView(R.id.detail_location_adapter_so2)
    TextView detail_location_so2;

    @BindView(R.id.favori_icon)
    ImageView favori_icon;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_location);

        ButterKnife.bind(this);

        Location location = (Location) getIntent().getSerializableExtra("location");

        String apiKey = "AIzaSyDi50_kgpXRHu6Gwc2fzPORXzDoZfh3r6A";

        String imageUrl = "https://maps.googleapis.com/maps/api/streetview?size=400x400&location="+location.getCoordinates().latitude+","+location.getCoordinates().longitude+"&key="+apiKey;

        Picasso.get().load(imageUrl).into(detail_location_image);

        detail_location_name.setText(location.location);

        SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
        SimpleDateFormat outputFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        Date date = new Date();
        try {
            date = inputFormat.parse(location.lastUpdated);
        }catch (ParseException p){}

        String formattedDate = outputFormat.format(date);
        System.out.println(formattedDate);

        detail_location_date.setText(formattedDate);

        if(location.favori==true)
        {
            favori_icon.setImageResource(R.drawable.icone_retirer_favoris);
        }

        // mise en place de click permettant de metter la location en favoris
        favori_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Open place details activity
                ActiveAndroid.beginTransaction();

                if(location.favori == true)
                {
                    favori_icon.setImageResource(R.drawable.icone_ajouter_favoris);
                    location.favori = false;
                }
                else
                {
                    favori_icon.setImageResource(R.drawable.icone_retirer_favoris);
                    location.favori = true;
                }
                location.save();
                ActiveAndroid.setTransactionSuccessful();
                ActiveAndroid.endTransaction();
            }
        });

        for(Measure measure : location.measures)
        {
            switch (measure.parameter) {
                case "pm25":
                    detail_location_pm25.setText(measure.parameter.toUpperCase() + " : " + measure.value + " " + measure.unit);
                    break;
                case "pm10":
                    detail_location_pm10.setText(measure.parameter.toUpperCase() + " : " + measure.value + " " + measure.unit);
                    break;
                case "so2":
                    detail_location_so2.setText(measure.parameter.toUpperCase() + " : " + measure.value + " " + measure.unit);
                    break;
                case "no2":
                    detail_location_no2.setText(measure.parameter.toUpperCase() + " : " + measure.value + " " + measure.unit);
                    break;
                case "o3":
                    detail_location_o3.setText(measure.parameter.toUpperCase() + " : " + measure.value + " " + measure.unit);
                    break;
                case "co":
                    detail_location_co.setText(measure.parameter.toUpperCase() + " : " + measure.value + " " + measure.unit);
                    break;
                case "bc":
                    detail_location_bc.setText(measure.parameter.toUpperCase() + " :" + measure.value + " " + measure.value);
                    break;
                default:

            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

    }
}
