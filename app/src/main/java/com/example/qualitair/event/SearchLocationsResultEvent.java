package com.example.qualitair.event;

import com.example.qualitair.model.Location.Location;
import com.example.qualitair.model.Zone.Zone;

import java.util.List;

/**
 * Created by alexmorel on 10/01/2018.
 */

public class SearchLocationsResultEvent {

    private List<Location> locations;

    public SearchLocationsResultEvent(List<Location> locations) {
        this.locations = locations;
    }

    public List<Location> getLocations() {
        return locations;
    }
}
