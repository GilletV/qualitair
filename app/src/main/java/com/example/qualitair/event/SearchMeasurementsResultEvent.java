package com.example.qualitair.event;

import com.example.qualitair.model.Location.Location;
import com.example.qualitair.model.Measure.Measure;
import com.google.gson.annotations.Expose;

import java.util.List;

public class SearchMeasurementsResultEvent {

    private List<Measure> measures;

    private Integer index;

    public SearchMeasurementsResultEvent(List<Measure> measures, Integer index) {
        this.measures = measures;
        this.index = index;
    }

    public List<Measure> getMeasures() {
        return measures;
    }

    public Integer getIndex() {
        return index;
    }

}
