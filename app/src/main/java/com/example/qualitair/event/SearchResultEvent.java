package com.example.qualitair.event;

import com.example.qualitair.model.Zone.Zone;

import java.util.List;

/**
 * Created by alexmorel on 10/01/2018.
 */

public class SearchResultEvent {

    private List<Zone> zones;

    public SearchResultEvent(List<Zone> zones) {
        this.zones = zones;
    }

    public List<Zone> getZones() {
        return zones;
    }
}
